import 'package:flutter/material.dart';

abstract class ListItem {}

class HeadingItem implements ListItem {
  final String heading;

  HeadingItem(this.heading);
}

class MessageItem implements ListItem {
  final String sender;
  final String body;

  MessageItem(this.sender, this.body);
}

class Todo {
  final String title;
  final String description;

  Todo(this.title, this.description);
}


void main() => runApp(GestureApp());


class GestureApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
            title: Text("Gesture")
        ),
        body: Center(
          child: GestureDetector(
            onTap: () {
              print("onTap");
            },
            onTapDown: (TapDownDetails) {
              print("onTapDown");
            },
            onTapUp: (TapUpDetails) {
              print("onTapUp");
            },
            onTapCancel: () {
              print("onTapCancel");
            },
            onDoubleTap: () {
              print("onDoubleTap");
            },
            onLongPress: () {
              print("onLongPress");
            },
            onVerticalDragStart: (DragStartDetails) {
              print("onVerticalDragStart");
            },
            onVerticalDragUpdate: (DragUpdateDetails) {
              print("onVerticalDragUpdate");
            },
            onVerticalDragEnd: (DragEndDetails) {
              print("onVerticalDragEnd");
            },
            onHorizontalDragStart: (DragStartDetails) {
              print("onHorizontalDragStart");
            },
            onHorizontalDragUpdate: (DragUpdateDetails) {
              print("onHorizontalDragUpdate");
            },
            onHorizontalDragEnd: (DragEndDetails) {
              print("onHorizontalDragEnd");
            },
            child: Container(
                width: 200.0,
                height: 200.0,
                color: Colors.yellow,
                child: Center(
                    child: Text("Wrapper")
                )
            ),
          ),
        ),
      ),
    );
  }
}

class BuildingLayoutApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
                title: Text("Building Layout")
            ),
            body: ListView(
              children: [
                BuildingLayoutAppImageSection(),
                BuildingLayoutAppTitleSection(),
                BuildingLayoutAppButtonRow(),
                BuildingLayoutAppTextSection()
              ],
            )
        )
    );
  }
}

class BuildingLayoutAppImageSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "images/lake.jpg"
    );
  }
}

class BuildingLayoutAppTitleSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(32.0),
      child: Row(
        children: [
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: EdgeInsets.only(bottom: 8.0),
                      child: Text(
                          "Oeschinen Lake Campground",
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          )
                      )
                  ),
                  Text(
                      "Kandersteg, Switzerland",
                      style: TextStyle(
                          color: Colors.grey[500]
                      )
                  )
                ],
              ),
          ),
          BuildingLayoutAppFavoriteWidget()
        ],
      ),
    );
  }
}

class BuildingLayoutAppFavoriteWidget extends StatefulWidget {
  @override
  _BuildingLayoutAppFavoriteWidgetState createState() => _BuildingLayoutAppFavoriteWidgetState();
}

class _BuildingLayoutAppFavoriteWidgetState extends State<BuildingLayoutAppFavoriteWidget> {
  bool isFavorited = false;
  int favoriteCount = 40;

  void toggleFavorite() {
    setState(() {
      if (isFavorited) {
        favoriteCount -= 1;
      } else {
        favoriteCount += 1;
      }
      isFavorited = !isFavorited;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
            children: [
              IconButton(
                icon: Icon(isFavorited ? Icons.star : Icons.star_border),
                color: isFavorited ? Colors.red[500] : Colors.grey[500],
                onPressed: toggleFavorite,
              ),
              Text(favoriteCount.toString())
            ]
        )
    );
  }
}

class BuildingLayoutAppButtonRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Column buildButtonColumn(IconData icon, String label) {
      Color color = Theme
          .of(context)
          .primaryColor;

      return Column(
          children: [
            Icon(
                icon,
                color: color
            ),
            Container(
              margin: EdgeInsets.only(top: 8.0),
              child: Text(
                  label,
                  style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400,
                      color: color
                  )
              ),
            )
          ]
      );
    }

    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          buildButtonColumn(Icons.call, "CALL"),
          buildButtonColumn(Icons.near_me, "ROUTE"),
          buildButtonColumn(Icons.share, "SHARE")
        ]
    );
  }
}

class BuildingLayoutAppTextSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(32.0),
      child: Text(
        'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese Alps. Situated 1,578 meters above sea level, it is one of the larger Alpine Lakes. A gondola ride from Kandersteg, followed by a half-hour walk through pastures and pine forest, leads you to the lake, which warms to 20 degrees Celsius in the summer. Activities enjoyed here include rowing, and riding the summer toboggan run.',
      ),
    );
  }
}

class DataTableApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
                title: Text("DataTable")
            ),
            body: SampleDataTable()
        )
    );
  }
}

class SampleDataTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new DataTable(
        columns: <DataColumn>[
          new DataColumn(
              label: new Text("AAAAA")
          ),
          new DataColumn(
              label: new Text("BBBBB")
          ),
          new DataColumn(
              label: new Text("CCCCC")
          ),
          new DataColumn(
              label: new Text("DDDDD")
          )
        ],
        rows: <DataRow>[
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A1")
                ),
                new DataCell(
                    new Text("B1")
                ),
                new DataCell(
                    new Text("C1")
                ),
                new DataCell(
                    new Text("D1")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A2")
                ),
                new DataCell(
                    new Text("B2")
                ),
                new DataCell(
                    new Text("C2")
                ),
                new DataCell(
                    new Text("D2")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A3")
                ),
                new DataCell(
                    new Text("B3")
                ),
                new DataCell(
                    new Text("C3")
                ),
                new DataCell(
                    new Text("D3")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A4")
                ),
                new DataCell(
                    new Text("B4")
                ),
                new DataCell(
                    new Text("C4")
                ),
                new DataCell(
                    new Text("D4")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A5")
                ),
                new DataCell(
                    new Text("B5")
                ),
                new DataCell(
                    new Text("C5")
                ),
                new DataCell(
                    new Text("D5")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A6")
                ),
                new DataCell(
                    new Text("B6")
                ),
                new DataCell(
                    new Text("C6")
                ),
                new DataCell(
                    new Text("D6")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A7")
                ),
                new DataCell(
                    new Text("B7")
                ),
                new DataCell(
                    new Text("C7")
                ),
                new DataCell(
                    new Text("D7")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A8")
                ),
                new DataCell(
                    new Text("B8")
                ),
                new DataCell(
                    new Text("C8")
                ),
                new DataCell(
                    new Text("D8")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A9")
                ),
                new DataCell(
                    new Text("B9")
                ),
                new DataCell(
                    new Text("C9")
                ),
                new DataCell(
                    new Text("D9")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A10")
                ),
                new DataCell(
                    new Text("B10")
                ),
                new DataCell(
                    new Text("C10")
                ),
                new DataCell(
                    new Text("D10")
                )
              ]
          ),          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A11")
                ),
                new DataCell(
                    new Text("B11")
                ),
                new DataCell(
                    new Text("C11")
                ),
                new DataCell(
                    new Text("D11")
                )
              ]
          ),
          new DataRow(
              cells: <DataCell>[
                new DataCell(
                    new Text("A12")
                ),
                new DataCell(
                    new Text("B12")
                ),
                new DataCell(
                    new Text("C12")
                ),
                new DataCell(
                    new Text("D12")
                )
              ]
          )
        ]
    )
    ;
  }
}

class NamedRouteWithDataApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new NamedRouteWithDataHomeScreen(),
      onGenerateRoute: (routeSettings) {
        var path = routeSettings.name.split('/');
        if (path[1] == "first") {
          final data = path[2];
          return new MaterialPageRoute(
            builder: (context) => new NamedRouteWithDataFirstScreen(data: data),
            settings: routeSettings,
          );
        }
        if (path[1] == "second") {
          final data = path[2];
          return new MaterialPageRoute(
            builder: (context) => new NamedRouteWithDataSecondScreen(data: data),
            settings: routeSettings,
          );
        }
        // fallback route here
      }
    );
  }
}

class NamedRouteWithDataHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Home"),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/first/homeToFirst');
              },
              child: new Text('Go to first 1'),
            ),
            new RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/second/homeToSecond');
              },
              child: new Text('Go to second 2'),
            )
          ],
        )
      ),
    );
  }
}

class NamedRouteWithDataFirstScreen extends StatelessWidget {
  final String data;

  NamedRouteWithDataFirstScreen({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("1"),
      ),
      body: new Center(
        child: new RaisedButton(
          onPressed: () {
            print(data);
            Navigator.pushNamed(context, '/second/firstToSecond');
          },
          child: new Text('Go to second screen'),
        ),
      ),
    );
  }
}

class NamedRouteWithDataSecondScreen extends StatelessWidget {
  final String data;

  NamedRouteWithDataSecondScreen({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("2"),
      ),
      body: new Center(
        child: new RaisedButton(
          onPressed: () {
            print(data);
            Navigator.pushNamed(context, '/first/secondToFirst');
          },
          child: new Text('Go to first screen'),
        ),
      ),
    );
  }
}

class RouteFirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('First Screen'),
      ),
      body: new Center(
        child: new RaisedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/second');
          },
          child: new Text('Launch new screen'),
        ),
      ),
    );
  }
}

class RouteSecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Second Screen"),
      ),
      body: new Center(
        child: new RaisedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
          child: new Text('Go back!'),
        ),
      ),
    );
  }
}

class ReturnValueHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Returning Data Demo'),
      ),
      body: new Center(child: new ReturnValueSelectionButton()),
    );
  }
}

class ReturnValueSelectionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      onPressed: () {
        navigateAndDisplaySelection(context);
      },
      child: new Text("Pick an option, any option!")
    );
  }

  navigateAndDisplaySelection(BuildContext context) async {
    final result = await Navigator.push(
      context,
      new MaterialPageRoute(
          builder: (context) => new ReturnValueSelectionScreen()
      )
    );

    print(result);
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text("$result")
    ));
  }
}

class ReturnValueSelectionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Pick an option")
      ),
      body: new Center (
        child: new Column(
          children: <Widget>[
            new RaisedButton(
              onPressed: () {
                Navigator.pop(context, "Yep!");
              },
              child: new Text("Yep!")
            ),
            new RaisedButton(
              onPressed: () {
                Navigator.pop(context, "Nope.");
              },
              child: new Text("Nope.")
            )
          ]
        )
      )
    );
  }
}

class TodoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "TodoApp Demo",
      home: new TodoScreen(
        todos: new List.generate(
          20,
              (i) => new Todo(
            'Todo $i',
            'A description of what needs to be done for Todo $i',
          ),
        )
      )
    );
  }
}

class TodoScreen extends StatelessWidget {
  final List<Todo> todos;

  TodoScreen({Key key, @required this.todos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("TodoScreen")
        ),
        body: new ListView.builder(
          itemCount: todos.length,
          itemBuilder: (context, index) {
            return new ListTile(
              title: new Text(todos[index].title),
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new DetailScreen(
                        todo: todos[index]
                    ),
                  )
                );
              }
            );
          }
        )
    );
  }
}

class DetailScreen extends StatelessWidget {
  final Todo todo;

  DetailScreen({Key key, @required this.todo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
            title: new Text(todo.title)
        ),
        body: new Text(todo.description)
    );
  }
}

class FirstSecondScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new FirstScreen()
    );
  }
}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('First Screen'),
      ),
      body: new Center(
        child: new RaisedButton(
          child: new Text('Launch new screen'),
          onPressed: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new SecondScreen()
                )
            );
          }
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Second Screen"),
      ),
      body: new Center(
        child: new RaisedButton(
          child: new Text('Go back!'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

class RetrieveTextDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Retrieve Text Input',
      home: new Scaffold(
        appBar: new AppBar(
            title: new Text('Retrieve Text Input')
        ),
        body: new MyForm()
      )
    );
  }
}

class MyForm extends StatefulWidget {
  @override
  MyFormState createState() => new MyFormState();
}

class MyFormState extends State<MyForm> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return new Form(
        key: _formKey,
        child: new Column(
          children: <Widget>[
            new TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
              },
            ),
            new RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  print('Processing Data');
                  Scaffold.of(context).showSnackBar(new SnackBar(content: new Text('Processing Data')));
                }
              },
              child: new Text('Submit'),
            )
          ],
        )
    );
  }
}

class TextFieldDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('TextFieldsDemo'),
        ),
        body: new TextFormField(
          decoration: new InputDecoration(
              hintText: 'Please enter a search term'
          ),
        )
      )
    );
  }
}

class DismissibleDemo extends StatelessWidget {
  final List<String> items;

  DismissibleDemo({Key key, @required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: new ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            final item = items[index];
            
            return new Dismissible(
                key: new Key(item),
                child: new ListTile(
                  title: new Text('$item')
                ),
                background: new Container(color: Colors.red),
                direction: DismissDirection.endToStart,
                onDismissed: (direction) {
                  items.removeAt(index);

                  Scaffold.of(context).showSnackBar(
                      new SnackBar(content: new Text("$direction"))
                  );
                }
            );
          },
        )
      )
    );
  }
}

class GridViewDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: new GridView.count(
          crossAxisCount: 3,
          children: new List.generate(
            100,
            (index) {
              return new Center(
                child: new Text('Item $index')
              );
            }
          ),
        )
      )
    );
  }
}

class MixedListDemo extends StatelessWidget {
  final List<ListItem> items;

  MixedListDemo({Key key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'MixedListDemo',
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('MixedList')
        ),
        body: new ListView.builder(
          itemBuilder: (context, index) {
            final item = items[index];

            if (item is HeadingItem) {
              return new ListTile(
                title: new Text(
                  item.heading,
                ),
              );
            } else if (item is MessageItem) {
              return new ListTile(
                title: new Text(item.sender),
                subtitle: new Text(item.body),
              );
            }
          },
        )
      )
    );
  }

}

class LongListViewDemo extends StatelessWidget {
  final List<String> items;

  LongListViewDemo({Key key, @required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('LongList Demo')
        ),
        body: new ListView.builder(
          itemBuilder: (context, index) {
            return ListTile(
              title: new Text('${items[index]}')
            );
          }
        )
      )
    );
  }
}

class ListViewDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: new ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            new Container(
              width: 160.0,
              color: Colors.red
            ),
            new Container(
              width: 160.0,
              color: Colors.blue
            ),
            new Container(
              width: 160.0,
              color: Colors.green
            ),
          ],
        )
      )
    );
  }
}

class DrawerDemo extends StatelessWidget {
  final appTitle = 'Drawer Demo';

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: appTitle,
      home: DrawerDemoHomePage()
    );
  }
}

class DrawerDemoHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: new Text('DrawerDemoHomePage')),
        body: new Center(
            child: new Text('Drawer')
        ),
        drawer: new Drawer(
            child: new ListView(
              children: <Widget>[
                new DrawerHeader(
                  child: new Text('DrawerHeader'),
                  decoration: new BoxDecoration(
                    color: Colors.blue,
                  ),
                ),
                new ListTile(
                    title: new Text('Item1'),
                    onTap: () {
                      Navigator.pop(context);
                    }
                ),
              ],
            )
        )
    );
  }
}

class TabBarDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new DefaultTabController(
          length: 3,
          child: new Scaffold(
            appBar: new AppBar(
              bottom: new TabBar(
                tabs: [
                  new Tab(icon: new Icon(Icons.directions_car)),
                  new Tab(icon: new Icon(Icons.directions_transit)),
                  new Tab(icon: new Icon(Icons.directions_bike))
                ]
              ),
              title: new Text('Tabs Demo'),
            ),
            body: new TabBarView(
              children: [
                new Icon(Icons.directions_car),
                new Icon(Icons.directions_transit),
                new Icon(Icons.directions_bike)
              ]
            )
          )
      )
    );
  }
}

class SnackBarDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'SnackBar Demo',
        home: new Scaffold(
            appBar: new AppBar(
              title: new Text('SnackBar Demo'),
            ),
            body: new SnackBarPage()));
  }
}

class SnackBarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Center(
        child: new RaisedButton(
            onPressed: () {
              final snackBar =
                  new SnackBar(content: new Text('Yay! A SnackBar!'));
              Scaffold.of(context).showSnackBar(snackBar);
            },
            child: new Text('Show SnackBar')));
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              'Hello!! You have pushed the button this many times:',
            ),
            new Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: new Theme(
          data: Theme.of(context).copyWith(accentColor: Colors.pink),
          child: new FloatingActionButton(
            onPressed: () {
              setState(() {
                _counter++;
              });
              final snackBar =
                  new SnackBar(content: new Text('Yay! A SnackBar!'));
              Scaffold.of(context).showSnackBar(snackBar);
            },
            tooltip: 'Increment',
            child: new Icon(Icons.add),
          )),
    );
  }
}
